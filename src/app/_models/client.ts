export class client {
    address: string;
    birthday: number;
    created_at: number;
    deleted_at: number;
    first_name: string;
    last_name: string;
    id:number;
    phone:number;
    updated_at : number;
    user_id:number;
}
