﻿
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {DataTableModule} from "angular2-datatable";
import { CustomFormsModule } from 'ng2-validation';
import { PdfViewerModule } from 'ng2-pdf-viewer';


// used to create fake backend
import { fakeBackendProvider } from './_helpers/index';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';
import { AlertComponent } from './_directives/index';
import { AuthGuard } from './_guards/index';
import { JwtInterceptor } from './_helpers/index';
import { AlertService, AuthenticationService, UserService } from './_services/index';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { CustomersComponent } from './customers/customers.component';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { SurveysComponent } from './surveys/surveys.component';
import { CreateSurveyComponent } from './create-survey/create-survey.component';
import { UpdateCustomerComponent } from './update-customer/update-customer.component';
import { UpdateSurveyComponent } from './update-survey/update-survey.component';
import { QuestionComponent } from './question/question.component';
import { CreateQuestionComponent } from './create-question/create-question.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { UpdateQuestionComponent } from './update-question/update-question.component';
import { CreateAnswerComponent } from './create-answer/create-answer.component';
import { UpdateAnswerComponent } from './update-answer/update-answer.component';


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        routing,
        DataTableModule,
        CustomFormsModule,
        PdfViewerModule,
       
        
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        CustomersComponent,
        CreateCustomerComponent,
        SurveysComponent,
        CreateSurveyComponent,
        UpdateCustomerComponent,
        UpdateSurveyComponent,
        QuestionComponent,
        CreateQuestionComponent,
        EditUserComponent,
        UpdateQuestionComponent,
        CreateAnswerComponent,
        UpdateAnswerComponent,
    
    ],
    providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        }, 

        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent ]
})

export class AppModule { }