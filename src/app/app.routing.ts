﻿import { UpdateAnswerComponent } from './update-answer/update-answer.component';
import { CreateAnswerComponent } from './create-answer/create-answer.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { CreateQuestionComponent } from './create-question/create-question.component';
import { QuestionComponent } from './question/question.component';
import { UpdateSurveyComponent } from './update-survey/update-survey.component';
import { CreateSurveyComponent } from './create-survey/create-survey.component';
import { SurveysComponent } from './surveys/surveys.component';

import { CustomersComponent } from './customers/customers.component';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { AuthGuard } from './_guards/index';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { UpdateCustomerComponent } from './update-customer/update-customer.component';
import { UpdateQuestionComponent } from './update-question/update-question.component';






const appRoutes: Routes = [
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent,  },
    { path: 'register', component: RegisterComponent },
    { path: 'customers', component: CustomersComponent },
    { path: 'create_customer', component: CreateCustomerComponent },
    { path: 'surveys', component: SurveysComponent },
    { path: 'create_survey', component: CreateSurveyComponent },
    { path: 'update_customer/:id', component: UpdateCustomerComponent },
    { path: 'update_survey', component: UpdateSurveyComponent },
    { path: 'question', component: QuestionComponent },
    { path: 'create-question', component: CreateQuestionComponent },
    { path: 'edit-user', component: EditUserComponent },
    { path: 'update-question', component: UpdateQuestionComponent },
    { path: 'create-answer', component: CreateAnswerComponent },
    { path: 'update-answer', component: UpdateAnswerComponent },


 
    // otherwise redirect to home
    { path: '**', redirectTo: 'login' }
];

export const routing = RouterModule.forRoot(appRoutes);