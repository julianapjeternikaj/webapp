import { answer } from './../_models/answer';
import { question } from './../_models/question';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { Router } from '@angular/router';




@Component({
  selector: 'app-create-answer',
  templateUrl: './create-answer.component.html',
  styleUrls: ['./create-answer.component.css']
})
export class CreateAnswerComponent implements OnInit {
  model: any = {};
  questions: question;
  answers: answer;

  constructor( 
    private userService: UserService, 
    private http: HttpClient, 
    private router: Router) { }

  ngOnInit() {
    this.answers = JSON.parse(localStorage.getItem('answers'));
  }

  newanswer() {
    let request = { "description_answer": this.model.description_answer,  
                    "question_id": this.answers.id,
                   }; 
                 
                   this.http.post(
                    "http://amd.webapp.al/answer/", 
                    request
                    )
                    .subscribe(data => {
                          console.log(data);
                          this.router.navigate(['./question'])
                    })
                    
                }

}
