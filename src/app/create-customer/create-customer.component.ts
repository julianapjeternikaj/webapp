import { Component, OnInit } from '@angular/core';
import { AlertService, UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../_models/index';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css']
})
export class CreateCustomerComponent implements OnInit {
  model: any = {};
  currentUser: User;
  
    constructor(private http: HttpClient,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService) {
      
     }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  newcustomer() {
    let request = { "first_name": this.model.first_name, 
                    "last_name": this.model.last_name,  
                    "phone": this.model.phone, 
                    "address": this.model.address,
                    "birthday": this.model.birthday, 
                    "user_id": this.currentUser.id,
    };
    this.http.post(
      "http://amd.webapp.al/customers", 
      request
      )
      .subscribe(data => {
            console.log(data);
            this.router.navigate(['./customers'])
      })

  }

}
