import { survey } from './../_models/index';
import { question } from './../_models/question';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { Router } from '@angular/router';
import { User } from './../_models/user';


@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.css']
})
export class CreateQuestionComponent implements OnInit {
  currentUser: User;
  model: any = {};
  questions: question;

  

  constructor(private userService: UserService, private http: HttpClient, private router: Router) 
     { }

     ngOnInit() {
      this.questions = JSON.parse(localStorage.getItem('questions'));
    }
  

  newquestion() {
    let request = { "description_question": this.model.description_question, 
                    "type_question": this.model.type_question,  
                    "survey_id": this.questions.id,
                   }; 
                 
                   this.http.post(
                    "http://amd.webapp.al/question/", 
                    request
                    )
                    .subscribe(data => {
                          console.log(data);
                          this.router.navigate(['./question'])
                    })
                    
                }

               
            
}
