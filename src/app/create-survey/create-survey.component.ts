
import { User } from './../_models/user';
import { survey } from './../_models/index';
import { Component, OnInit } from '@angular/core';
import { AlertService, UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';



@Component({
  selector: 'app-create-survey',
  templateUrl: './create-survey.component.html',
  styleUrls: ['./create-survey.component.css']
})
export class CreateSurveyComponent implements OnInit {
  model: any = {};
  editSurvey: survey;



  constructor( private http: HttpClient,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.editSurvey = JSON.parse(localStorage.getItem('editSurvey'));
  }

  newsurvey() {
    let request = { "description": this.model.description, 
                    "type_survey": this.model.type_survey,  
                    "start_date": this.model.start_date, 
                    "end_date": this.model.end_date,
                    "customer_id": this.editSurvey.id,
    };

    this.http.post(
      "http://amd.webapp.al/survey", 
      request
      )
      .subscribe(data => {
            console.log(data);
            this.router.navigate(['./surveys'])
      })

    }
}
