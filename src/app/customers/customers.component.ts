import { User } from './../_models/user';
import { Component, OnInit } from '@angular/core';
import { client } from '../_models/index';
import { UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { Router } from '@angular/router';




@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
  
})
export class CustomersComponent implements OnInit {
  currentUser: User;
  clients: client[];

  constructor(private userService: UserService, private http: HttpClient, private router: Router) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
}

  ngOnInit() {
    this.getAllCustomers();
  }

  private getAllCustomers(){
    this.http.get(" http://amd.webapp.al/customers/get-all-customers", {})
    .subscribe(data=> {
        this.clients = data as client[];
        console.log(this.clients);
})
   
}
 Myfunction1(){
 this.router.navigate(['./create_customer']);
}


Showuserdetails(customerId){
  for(let i=0; i < this.clients.length; i++){
    if( this.clients[i].id == customerId){
      localStorage.setItem("editUser", JSON.stringify(this.clients[i]));   
    }
  }
  
  this.router.navigate(['./update_customer', customerId]);
 }

 Showsurveys(){
  this.router.navigate(['./surveys']);
 }


}

