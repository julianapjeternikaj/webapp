import { Component, OnInit } from '@angular/core';
import { User } from '../_models/index';
import { UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { Router , ActivatedRoute, ParamMap} from '@angular/router';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  currentUser: User;
  users: User[] = [];

  constructor(
       private userService: UserService, 
       private http: HttpClient , 
       private route: ActivatedRoute,  
       private router: Router) 
       { 
       this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      }

  ngOnInit() {
  }

  private updateUser(){
    this.http.patch("http://amd.webapp.al/question/" + this.currentUser.id, this.currentUser)
    .subscribe(data=> {
        console.log(data);
        this.router.navigate(['./home']);
    })
  }
}
