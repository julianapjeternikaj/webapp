﻿import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, AuthenticationService } from '../_services/index';
import { stringify } from '@angular/core/src/util';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'login.component.html'
})

@Injectable()
export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private http: HttpClient
    ) { }

    ngOnInit() {
    

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }


    login() {
        let request = { "email": this.model.email, 
                        "password": this.model.password, 
                        
        };

        this.http.post("http://amd.webapp.al/login", request)
            .subscribe(data => {
                localStorage.setItem("token", data['token'] );
                localStorage.setItem("currentUser", JSON.stringify(data['user'])  );
                this.router.navigate(['/home']);
        })
    }
}


