import { answer } from './../_models/answer';
import { question } from './../_models/question';
import { CreateSurveyComponent } from './../create-survey/create-survey.component';
import { Component, OnInit } from '@angular/core';
import { AlertService, UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  questions: question[];
  answers: answer[];


  constructor  (  
    private http: HttpClient,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService) 
    {
    this.questions = JSON.parse(localStorage.getItem('questions'));
    this.answers = JSON.parse(localStorage.getItem('answers'));
     }

  ngOnInit() {
    this.getAllQuestions();
    this.getAllAnswers();
  
  }

  private getAllQuestions(){
    this.http.get("http://amd.webapp.al/get_all_questions", {})
    .subscribe(data=> {
       this.questions = data as question[];
        console.log(data)

    });
  }
  private getAllAnswers(){
    this.http.get("http://amd.webapp.al/get_all_answers", {})
    .subscribe(data=> {
      this.answers = data as answer[];
      console.log(data);
    });
  }

  Createquestion(){
    this.router.navigate(['./create-question']);
   }
   Createanswer(){
    this.router.navigate(['./create-answer']);
   }

    Editquestion(id){
    console.log(id);
    for(let i=0; i < this.questions.length; i++){
      if( this.questions[i].id == id){
        localStorage.setItem("questions", JSON.stringify(this.questions[i]));   
    }
   }
    
    this.router.navigate(['./update-question', ]);
   }

   Editanswer(id){
    for(let i=0; i < this.answers.length; i++){
      if( this.answers[i].id == id){
        localStorage.setItem("answers", JSON.stringify(this.answers[i]));   
    }
   }
    
    this.router.navigate(['./update-answer', ]);
   }





}
