﻿ import { Component } from '@angular/core';
 import { Router } from '@angular/router';
 import { Injectable } from '@angular/core';

 import { AlertService, UserService } from '../_services/index';
 import { HttpClient, HttpHeaders } from '@angular/common/http';

 @Component({
     moduleId: module.id.toString(),
     templateUrl: 'register.component.html'
 })

 @Injectable()
 export class RegisterComponent {
     model: any = {};
     loading = false;

     constructor(
         private http: HttpClient,
         private router: Router,
         private userService: UserService,
         private alertService: AlertService) { }


         register() {
         let request = { "username": this.model.username, 
                         "password": this.model.password,  
                         "email": this.model.email, 
                         "first_name": this.model.firstName,
                         "last_name": this.model.lastName 
         };

         this.http.post(
             "http://amd.webapp.al/signup", 
             request
             )
             .subscribe(data => {
                  localStorage.setItem("token", data['token']);   
                  this.router.navigate(['/login']);
                  console.log(data);

       })
     } 
    }
