import { QuestionComponent } from './../question/question.component';
import { CreateSurveyComponent } from './../create-survey/create-survey.component';
import { Component, OnInit , ElementRef } from '@angular/core';
import { AlertService, UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { survey } from '../_models/index';
import { User } from './../_models/user';
import * as jsPDF from 'jspdf';


@Component({
  selector: 'app-surveys',
  templateUrl: './surveys.component.html',
  styleUrls: ['./surveys.component.css']
})
export class SurveysComponent implements OnInit {
surveys: survey[] ;
customer_id ;
currentUser: User;
editSurvey: survey;
el: ElementRef;
questions;

  constructor( private http: HttpClient,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService,
    ) { 
    this.editSurvey = JSON.parse(localStorage.getItem('editSurvey'));
    }

  ngOnInit() {
    this.getAllSurveys();
   
  }
  
  private getAllSurveys(){
    this.http.get(" http://amd.webapp.al/get_all_surveys", {})
    .subscribe(data=> {
        this.surveys = data as survey[];
        console.log(this.surveys);
    });
  }

  private createsurvey() {
    this.router.navigate(['./create_survey']);
  } 
  

 
  Showsurveydetails(id){
    console.log(id);
    for(let i=0; i < this.surveys.length; i++){
      if( this.surveys[i].id == id){
        localStorage.setItem("editSurvey", JSON.stringify(this.surveys[i]));   
    }
   }
    
    this.router.navigate(['./update_survey', ]);
   }

   Showsurveys() {
      this.router.navigate(['./question', ]);
    }
    public Downloadpdf()  {
      let doc = new jsPDF();
      doc.text(20,20, this.questions = JSON.parse(localStorage.getItem('questions')));
      doc.save('Questions and answers.pdf');
      doc.setFont("courier");
      doc.setFontType("normal");
      doc.text(20, 30, 'This is courier normal.');
     }
    
   

}
