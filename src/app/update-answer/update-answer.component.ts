import { answer } from './../_models/answer';
import { question } from './../_models/question';
import { survey } from './../_models/index';
import { User } from './../_models/user';
import { Component, OnInit } from '@angular/core';
import { client } from '../_models/index';
import { UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { Router , ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-update-answer',
  templateUrl: './update-answer.component.html',
  styleUrls: ['./update-answer.component.css']
})
export class UpdateAnswerComponent implements OnInit {
  currentUser: User;
  question_id;
  answers:answer;
  survey_id;
  questions:question;

  constructor( private userService: UserService, private http: HttpClient, private route: ActivatedRoute,
    private router: Router) { 
      this.route.paramMap.switchMap((params: ParamMap) =>  this.question_id = params.get('id'));
    }

  ngOnInit() {
    this.getAnswerDetails(this.question_id);
  }
  private getAnswerDetails(question_id)
  {
    this.answers =  JSON.parse(localStorage.getItem("answers"));
    console.log(this.answers);
  }
  private updateAnswer(){
    this.http.patch("http://amdwebapp.al/answer/" + this.answers.id, this.answers)
    .subscribe(data=> {
        console.log(data);
        this.router.navigate(['./question']);
    })
  }
  private deleteAnswer(){
    this.http.delete(" http://amd.webapp.al/answer/" + this.answers.id, {})
    .subscribe(data=> {
        console.log(data);
        this.router.navigate(['./question']);
    })
  }

}
