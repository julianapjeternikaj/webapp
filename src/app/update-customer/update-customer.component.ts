
import { User } from './../_models/user';
import { Component, OnInit } from '@angular/core';
import { client } from '../_models/index';
import { UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { Router , ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-update-customer',
  templateUrl: './update-customer.component.html',
  styleUrls: ['./update-customer.component.css']
})
export class UpdateCustomerComponent implements OnInit {
  currentUser: User;
  client: client;
  customerId;


  constructor(private userService: UserService, private http: HttpClient, private route: ActivatedRoute,
    private router: Router) {
     
      this.route.paramMap.switchMap((params: ParamMap) =>  this.customerId = params.get('id'));
}

  ngOnInit() {
    this.getCustomerDetails(this.customerId);
  }

  private getCustomerDetails(customerId)
  {
    this.client =  JSON.parse(localStorage.getItem("editUser"));
    
  }

  private updateCustomer(){
    this.http.patch(" http://amd.webapp.al/customers/" + this.client.id, this.client)
    .subscribe(data=> {
        console.log(data);
        this.router.navigate(['./customers']);
    })
  }
  private deleteCustomer(){
    this.http.delete(" http://amd.webapp.al/customers/" + this.client.id, {})
    .subscribe(data=> {
        console.log(data);
        this.router.navigate(['./customers']);
    })
  }
 
}
