import { question } from './../_models/question';
import { survey } from './../_models/index';
import { User } from './../_models/user';
import { Component, OnInit } from '@angular/core';
import { client } from '../_models/index';
import { UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { Router , ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-update-question',
  templateUrl: './update-question.component.html',
  styleUrls: ['./update-question.component.css']
})
export class UpdateQuestionComponent implements OnInit {
  currentUser: User;
  question_id;
  survey:survey;
  survey_id;
  questions:question;
  constructor( private userService: UserService, private http: HttpClient, private route: ActivatedRoute,
    private router: Router) {
      this.route.paramMap.switchMap((params: ParamMap) =>  this.survey_id = params.get('id'));
     }

  ngOnInit() {
    this.getQuestionDetails(this.survey_id);
  }

  private getQuestionDetails(survey_id)
  {
    this.questions =  JSON.parse(localStorage.getItem("questions"));
    console.log(this.questions);
  }
  
  private updateQuestion(){
    this.http.patch("http://amd.webapp.al/question/" + this.questions.id, this.questions)
    .subscribe(data=> {
        console.log(data);
        this.router.navigate(['./question']);
    })
  }
  private deleteQuestion(){
    this.http.delete(" http://amd.webapp.al/question/" + this.questions.id, {})
    .subscribe(data=> {
        console.log(data);
        this.router.navigate(['./question']);
    })
  }

}
