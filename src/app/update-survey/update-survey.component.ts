
import { survey } from './../_models/index';
import { User } from './../_models/user';
import { Component, OnInit } from '@angular/core';
import { client } from '../_models/index';
import { UserService } from '../_services/index';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { Router , ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';


@Component({
  selector: 'app-update-survey',
  templateUrl: './update-survey.component.html',
  styleUrls: ['./update-survey.component.css']
})
export class UpdateSurveyComponent implements OnInit {
  currentUser: User;
  client: client;
  survey:survey;
  survey_id: string;
  customerId;
  editSurvey: survey;

  constructor( private userService: UserService, private http: HttpClient, private route: ActivatedRoute,
    private router: Router) {
      this.route.paramMap.switchMap((params: ParamMap) =>  this.customerId = params.get('id'));
     }

  ngOnInit() {
    this.getSurveydetails(this.customerId);
  }

  private getSurveydetails(customerID)
  {
    this.survey =  JSON.parse(localStorage.getItem("editSurvey"));
    
  }
  private updateSurvey(){
    this.http.patch(" http://amd.webapp.al/survey/" + this.survey.id, this.survey)
    .subscribe(data=> {
        console.log(data);
        this.router.navigate(['./surveys']);
    })
  }
  private deleteSurvey(){
    this.http.delete(" http://amd.webapp.al/survey/" + this.survey.id, {})
    .subscribe(data=> {
        console.log(data);
        this.router.navigate(['./surveys']);
    })
  }

}
